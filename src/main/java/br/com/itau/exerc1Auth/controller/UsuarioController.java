package br.com.itau.exerc1Auth.controller;

import br.com.itau.exerc1Auth.model.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController
{
    @GetMapping()
    public Usuario buscarInformacoesUsuarioAutenticado(@AuthenticationPrincipal Usuario usuarioAutenticado) {
        return usuarioAutenticado;
    }

}
