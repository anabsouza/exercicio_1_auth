package br.com.itau.exerc1Auth;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exerc1AuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(Exerc1AuthApplication.class, args);
	}

}
